# Acces a une base SQLite 3
import sqlite3
import datetime

# Creation/Ouverture d'une BDD
connect = sqlite3.connect("C:/Users/lecaroacosta/Documents/Formation_ALTRAN/Formation_Logiciel_2021/Gitlab/python/log.sqlite")

connect.execute(" CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY,info TEXT)")

# Ajout d'une valeur à la BDD
info = "Il est : " + str(datetime.datetime.now())
connect.execute("INSERT INTO messages(info) VALUES(?)", (info,))
connect.commit()

# Lecture dans la BDD
cursor = connect.cursor()
cursor.execute("SELECT * FROM messages")
for ligne in cursor.fetchall():
	print (ligne)
