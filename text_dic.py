"""Fichier qui test les classes"""
#******************************************************************************
#*************************"Formation Python : 17/03/2021"**********************
#******************************************************************************

print("---------------------------------------------")
# Exercise 1 CLASS: Attribute et Methode

# Créer une une classe capteur avec un nom, une valeur min, actuelle et max ; y créer une methode qui affiche le capteur sous la forme
# --->> "capteur 8 |##########---|" et tester

class capteur:
    """ Example de class 1.
    """
    nom = ""
    min = 0
    valeur = 0
    max = 100
    def afficher(self):
        """ Constructeur.
        """
        str = self.nom+" |"
        for n in range(10):
            if n<self.valeur * 10 / (self.max-self.min):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)
        
cl = capteur()
cl.valeur = 5
cl.afficher()

# CLASS avec constructeur

class capteur:
    """ Example de class 2
    """
#   Meilleur constructeur possible:
#   nom = ""#   min = 0#    max = 100#      valeur = 0
    def __init__(self, nom = "", min = 0, valeur = 0, max = 100):
        """ Constructeur 
        """
        self.nom = nom
        self.min = min
        self.valeur = valeur
        self.max = max
        
    def afficher(self):
        str = self.nom+" |"
        for n in range(10):
            if n<(self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)
        
cl = capteur()
cl.valeur = 66
cl.afficher()
c2 = capteur("Capteur 42", -5,2,5)

print("---------------------------------------------")
#Garbage Collector
# El espacio de la memoria es muy dificil de manejar y toma tiempo
# Puede tomar mucho tiempo en la ejecucion.
# Este se ejecuta en la ultima linea ex. linea 56:c2.afficher()
# Toma tiempo porque espera a que se libere la memoria, a causa de esta espera u
# optimizacion es demorada

print("---------------------------------------------")
#Add qqch sauf les nombres "+"
# class Personne:
# def __init__(self,n):
    # self.nom = n
    # self.age = 10
# def vieuillir(self): self.age += 1
# def __add__(self,v):
    # p2 = Personne(self.nom)
    # p2.age = self.age + v
    # return p2
    
class capteur:
    """ Example de class 3
    """
#   Meilleur constructeur possible:
#   nom = ""#   min = 0#    max = 100#      valeur = 0
    def __init__(self, nom = "", min = 0, valeur = 0, max = 100):
        """ Constructeur 
        """
        self.nom = nom
        self.min = min
        self.valeur = valeur
        self.max = max
        
    def afficher(self):
        str = self.nom+" |"
        for n in range(10):
            if n<(self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)
    
    #Permettre c1*3 et c1/3 (modifier tous les attributs) - "mul" et "truediv"
    def __mul__(self, v):
        self.min = self.min * v
        self.valeur = self.valeur * v
        self.max = self.max * v
        return capteur(self.nom,self.min,self.valeur,self.max)
    
    def __truediv__(self, v):
        self.min = self.min / v
        self.valeur = self.valeur / v
        self.max = self.max / v
        return capteur(self.nom,self.min,self.valeur,self.max)        
        
c1 = capteur("Capteur 42",-5,2,5)*3/2
c1.afficher()

print("---------------------------------------------")
# Heritage
# Une class A peut heriter une class B.
# Avoir une class A mere
# class Personne:
    # def __init__(self,n):
            # self.nom = n
            # self.age = 10
    # def vieuillir(self): self.age += 1
    # def __del__(self): print("del!")
# class Marin(Personne):
    # def __init__(self,n):
            # Personne.__init__(self,n)
            # self.bateau = ""
    
class capteur:
    """ Example de class 3
    """
#   Meilleur constructeur possible:
#   nom = ""#   min = 0#    max = 100#      valeur = 0
    def __init__(self, nom = "", min = 0, valeur = 0, max = 100):
        """ Constructeur 
        """
        self.nom = nom
        self.min = min
        self.valeur = valeur
        self.max = max
        
    def afficher(self):
        str = self.nom+" |"
        for n in range(10):
            if n<(self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)
    
    #Permettre c1*3 et c1/3 (modifier tous les attributs) - "mul" et "truediv"
    def __mul__(self, v):
        self.min = self.min * v
        self.valeur = self.valeur * v
        self.max = self.max * v
        return capteur(self.nom,self.min,self.valeur,self.max)
    
    def __truediv__(self, v):
        self.min = self.min / v
        self.valeur = self.valeur / v
        self.max = self.max / v
        return capteur(self.nom,self.min,self.valeur,self.max)

c1 = capteur("Capteur 42",-5,2,5)*4/2
c1.afficher()
print(c1.valeur) # 3

print("")
print("DOUBLECAPTEUR")
#Ajouter "DoubleCapteur", un Capteur avec une 2eme valeur qui soit superieur 
# ou egal a valeur

class DoubleCapteur(capteur):
    """ Example de class DoubleCapteur
    """
    def __init__(self, valeur2 = 0):
        capteur.__init__(self, nom = "", min = 0, valeur = 0, max = 100)
        self.valeur2 = valeur2
    
    def afficher(self,valeur2):
        str = self.nom+" |"
        for n in range(10):
            if n<(self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            elif n<(valeur2-self.min) * 10 / (self.max-self.min):
                str += "o"
            else:
                str += "-"
        str += "|"
        print(str)
        
    def __mul__(self, v):
        self.min = self.min * v
        self.valeur = self.valeur * v
        self.max = self.max * v
        return DoubleCapteur(self.nom,self.min,self.valeur,self.max)
    
    def __truediv__(self, v):
        self.min = self.min / v
        self.valeur = self.valeur / v
        self.max = self.max / v
        return DoubleCapteur(self.nom,self.min,self.valeur,self.max)

if __name__=="__main__"
c1 = capteur("Capteur 42",0,20,100)
dc1 = DoubleCapteur(0)
dc1.afficher(0)















