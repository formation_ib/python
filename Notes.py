#******************************************************************************
#*************************"Formation Python : 15/03/2021"**********************
#******************************************************************************
print("Formation Python : 15/03/2021")
    ********************START TEST CONSOLE***********************

    >>> a = none
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'none' is not defined
    >>> a = None
    >>> b = 12345
    >>> A
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'A' is not defined
    >>> a
    >>> b
    12345
    >>> b = 0.000000021454
    >>> b
    2.1454e-08
    >>> b = 123,111
    >>> b
    (123, 111)
    >>> b = 123 456
      File "<stdin>", line 1
        b = 123 456
                ^
    SyntaxError: invalid syntax
    >>> 12345654647987897987987897896545616574468413541864683135165
    12345654647987897987987897896545616574468413541864683135165
    >>> b=456461534845486746545313123416544684135415646845135
    >>> b
    456461534845486746545313123416544684135415646845135
    >>> e = "Super !"
    >>> e
    'Super !'
    >>> e = "Super !",dit-il
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'dit' is not defined
    >>> e = "\"Super !\", dit-il"
    >>> e
    '"Super !", dit-il'
    >>> e = "\"Super !\", dit-il\nPuis il parti."
    >>> e
    '"Super !", dit-il\nPuis il parti.'
    >>> print(e)
    "Super !", dit-il
    Puis il parti.
    >>> f = """OOO K K
    ... O O KK
    ... OOO K K"""
    >>> print(f)
    OOO K K
    O O KK
    OOO K K
    >>> d = true
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'true' is not defined
    >>> d = True
    >>> d
    True
    >>> d = 1
    >>> d = Flase
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'Flase' is not defined
    >>> d = False
    >>> d
    False
    >>> d = 1
    >>> d
    1
    >>> print("Dans e il y a :", e)
    Dans e il y a : "Super !", dit-il
    Puis il parti.
    >>> g = input("Ton nom ?")
    Ton nom ?Luis
    >>> g
    'Luis'
    >>> 9**0.5
    3.0
    >>> 169**0.5
    13.0
    >>> 17%5
    2
    >>> ~2
    -3
    >>> 5<<3
    40
    >>> ~3
    -4
    >>> ~0
    -1
    >>> not ((b>100000) and (-b>-100000))
    True
    >>> "abc"+"456"
    'abc456'
    >>> "a"*3
    'aaa'
    >>> b=45678979845348
    >>> b-=b
    >>> b
    0
    >>> b=456123416546
    >>> b-=b+1
    >>> b
    -1
    >>> if b < 10:
    ...     print("10")
    ... elif b < 0:
    ...     print(">0")
    ...
    10
    >>> if b < 10:
    ...     if b < 0:
    ...             print("a")
    ... else
      File "<stdin>", line 4
        else
            ^
    SyntaxError: invalid syntax
    >>> else:
      File "<stdin>", line 1
        else:
        ^
    SyntaxError: invalid syntax
    >>> if b < 10:
    ...     if b < 0:
    ...             print("a")
    ... else:
    ...     printf("c")
    ...
    a
    >>> if b < 10:
    ...     if b < 0:
    ...             print("a")
    ...     else
      File "<stdin>", line 4
        else
            ^
    SyntaxError: invalid syntax
    >>> b=5
    >>> if b < 10:
    ...     if b < 0:
    ...             print("a")
    ... else:
    ...     printf("c")
    ...
    >>> try print("ok")
      File "<stdin>", line 1
        try print("ok")
            ^
    SyntaxError: invalid syntax
    >>> try:
    ...     print("ok")
    ...     b=b/0
    ...     printf("ok 2")
    ... except ZeroDivisionError:
    ...     print(" / 0 ")
    ...
    ok
     / 0
    >>> try:
    ...     print("ok")
    ...     b=b/0
    ...     printf("ok 2")
    ... except ZeroDivisionError:
    ...     print(" / 0 ")
    ... except:
    ...     print("Erreur inattendue")
    ...
    ok
     / 0
    >>> try:
    ... ...     print("ok")
      File "<stdin>", line 2
        ...     print("ok")
        ^
    IndentationError: expected an indented block
    >>> ...     b=b/0
      File "<stdin>", line 1
        ...     b=b/0
                ^
    SyntaxError: invalid syntax
    >>> ...     printf("ok 2")
      File "<stdin>", line 1
        ...     printf("ok 2")
                ^
    SyntaxError: invalid syntax
    >>> ... except ZeroDivisionError:
      File "<stdin>", line 1
        ... except ZeroDivisionError:
            ^
    SyntaxError: invalid syntax
    >>> ...     print(" / 0 ")
      File "<stdin>", line 1
        ...     print(" / 0 ")
                ^
    SyntaxError: invalid syntax
    >>> ... except:
      File "<stdin>", line 1
        ... except:
            ^
    SyntaxError: invalid syntax
    >>> ...     print("Erreur inattendue")
      File "<stdin>", line 1
        ...     print("Erreur inattendue")
                ^
    SyntaxError: invalid syntax
    >>>

    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      printf("ok 2")
    ... except ZeroDivisionError:
    ...     print(" / 0 ")
    ...
    ok
     / 0
    >>> try:
    ...      b=b/0
    ...      printf("ok 2")
    ... ^Z

      File "<stdin>", line 3
        printf("ok 2")
                      ^
    SyntaxError: unexpected EOF while parsing
    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      printf("ok 2")
    ... except ZeroDivisionError as ex:
    ...     print(" / 0 ", ex)
    ...
    ok
     / 0  division by zero
    >>> try:
    ...      print("ok")
    ...      printf("ok 2")
    ...      b=b/0
    ... except ZeroDivisionError as ex:
    ...     print(" / 0 ", ex)
    ... finally:
    ...     print(".")
    ...
    ok
    .
    Traceback (most recent call last):
      File "<stdin>", line 3, in <module>
    NameError: name 'printf' is not defined
    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      printf("ok 2")
    ... except ZeroDivisionError as ex:
    ...     print(" / 0 ", ex)
    ... finally:
    ...
      File "<stdin>", line 8

        ^
    IndentationError: expected an indented block
    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      printf("ok 2")
    ... except ZeroDivisionError as ex:
    ...     printf(" / 0 ", ex)
    ...
    ok
    Traceback (most recent call last):
      File "<stdin>", line 3, in <module>
    ZeroDivisionError: division by zero

    During handling of the above exception, another exception occurred:

    Traceback (most recent call last):
      File "<stdin>", line 6, in <module>
    NameError: name 'printf' is not defined
    >>> try:
    ...      printf("ok")
    ...      b=b/0
    ...      printf("ok 2")
    ... except ZeroDivisionError as ex:
    ...      printf(" / 0 ", ex)
    ... finally:
    ...     printf(".")
    ...
    Traceback (most recent call last):
      File "<stdin>", line 2, in <module>
    NameError: name 'printf' is not defined

    During handling of the above exception, another exception occurred:

    Traceback (most recent call last):
      File "<stdin>", line 8, in <module>
    NameError: name 'printf' is not defined
    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      print("ok 2")
    ... except ZeroDivisionError as ex:
    ...      print(" / 0 ", ex)
    ... finally:
    ...     print(".")
    ...
    ok
     / 0  division by zero
    .
    >>> try:
    ...      printf("ok")
    ...
      File "<stdin>", line 3

        ^
    SyntaxError: invalid syntax
    >>> try:
    ...      b=b/0
    ...
      File "<stdin>", line 3

        ^
    SyntaxError: invalid syntax
    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      print("ok 2")
    ... except ZeroDivisionError as ex:
    ...      print(" / 0 ", ex)
    ... finally:
    ...     print(".")
    ...
    ok
     / 0  division by zero
    .
    >>> try:
    ...      print("ok")
    ...      b=b/0
    ...      print("ok 2")
    ... except ZeroDivisionError as ex:
    ...      print(" / 0 ", ex)
    ... else:
    ...     print("fin")
    ...
    ok
     / 0  division by zero
    >>>

    ********************FIN TEST CONSOLE***********************

    n=3
    while n < 20:
        print(n)
        n *= 2
        
    print("---------")
    for i in range(3):
        print(i)
     
    print("---------") 
    n=3
    while n<20:
        print(n)
        for i in range(3):
            n+=i
        
        
        
#******************************************************************************
#*************************"Formation Python : 16/03/2021"**********************
#******************************************************************************
print("Formation Python : 16/03/2021")

    >>> for i in range(2): print(i)
    ...
    0
    1
    >>> r=range(3)
    >>> r
    range(0, 3)
    >>> list(r)
    [0, 1, 2]
    >>> r2=range(4,8)
    >>> r2
    range(4, 8)
    >>> r3=range(5, 10, 2)
    >>> r3
    range(5, 10, 2)
    >>> -4 -7 -10
    -21
    >>> for i in range(-4,-11,-3): print(i)
    ...
    -4
    -7
    -10
    >>> for i in range(-4,-11,-3): print(-i)
    ...
    4
    7
    10
    >>> for i in range(4,11,3): print(-i)
    ...
    -4
    -7
    -10
    >>> for i in range(0,6,i): print(i)
    ...
    0
    >>> i=2
    >>> for i in range(0,6,i): print(i)
    ...
    0
    2
    4
    >>> l1 = ["a", "d","c","k"]
    >>> l1
    ['a', 'd', 'c', 'k']
    >>> l1[2]="o"
    >>> l1
    ['a', 'd', 'o', 'k']
    >>> l1[6]="o"
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    IndexError: list assignment index out of range
    >>> l1.append("z")
    >>> l1
    ['a', 'd', 'o', 'k', 'z']
    >>> del(l1[4])
    >>> l1
    ['a', 'd', 'o', 'k']
    >>> l1[0,2]
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: list indices must be integers or slices, not tuple
    >>> l1[0:2]
    ['a', 'd']
    >>> l1
    ['a', 'd', 'o', 'k']
    >>> l1[0:3:2]
    ['a', 'o']
    >>> l1[0:3:-2]
    []
    >>> l1[4:2:2]
    []
    >>> l1[3:0:-2]
    ['k', 'd']
    >>> l1[-2:]
    ['o', 'k']
    >>> l1[-1:-3]
    []
    >>> l1+["j","t","r"]
    ['a', 'd', 'o', 'k', 'j', 't', 'r']
    >>> l1+["j","t","r"]+str(f)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'f' is not defined
    >>> l1+["j","t","r"]+str("f")
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: can only concatenate list (not "str") to list
    >>> l1+["f"]
    ['a', 'd', 'o', 'k', 'f']
    >>> l1*3
    ['a', 'd', 'o', 'k', 'a', 'd', 'o', 'k', 'a', 'd', 'o', 'k']
    >>> for i in l1: print(i)
    ...
    a
    d
    o
    k
    >>> for v in l1: print(v)
    ...
    a
    d
    o
    k
    >>> l1
    ['a', 'd', 'o', 'k']
    >>> l1+=["f"]
    >>> l1
    ['a', 'd', 'o', 'k', 'f']
    >>> for i in l1: print(i)
    ...
    a
    d
    o
    k
    f
    >>> if "x" iin l1: print("trouve")
      File "<stdin>", line 1
        if "x" iin l1: print("trouve")
               ^
    SyntaxError: invalid syntax
    >>> if "x" in l1: print("trouve")
    ...
    >>> if "o" in l1: print("trouve")
    ...
    trouve
    >>> if "O" in l1: print("trouve")
    ...
    >>> l1=[2,2,3,2,2,3,2,2,2,2]
    >>> n=len(l1)
    >>> for i in range(n//2): print(i*2)
    ...
    0
    2
    4
    6
    8
    >>> for j in range(n): for i in range(n//2): print(i*j)
      File "<stdin>", line 1
        for j in range(n): for i in range(n//2): print(i*j)
                           ^
    SyntaxError: invalid syntax
    >>> for j in range(n): for i in range(n//2): print(i*j) # complexite O(n²/2)
      File "<stdin>", line 1
        for j in range(n): for i in range(n//2): print(i*j) # complexite O(n²/2)
                           ^
    SyntaxError: invalid syntax
    >>> for j in range(n//3): for i in range(n//2): print(i*j) # complexite O(n²/6)
      File "<stdin>", line 1
        for j in range(n//3): for i in range(n//2): print(i*j) # complexite O(n²/6)
                              ^
    SyntaxError: invalid syntax
    >>> for j in range(n): for i in range(n//5): print(i*j) # complexite O(n²/5) donc moins rapide
      File "<stdin>", line 1
        for j in range(n): for i in range(n//5): print(i*j) # complexite O(n²/5) donc moins rapide
                           ^
    SyntaxError: invalid syntax
    >>> for j in range(n): for i in range(j): print(i*j) # complexite O(n²/2)
      File "<stdin>", line 1
        for j in range(n): for i in range(j): print(i*j) # complexite O(n²/2)
                           ^
    SyntaxError: invalid syntax
    >>> values = [4, 2, 10, 9, 7, 8, 4, 3, 2, 4, 3, 9, 6, 3, 10, 11, 21]
    >>> values.sort()
    >>> values
    [2, 2, 3, 3, 3, 4, 4, 4, 6, 7, 8, 9, 9, 10, 10, 11, 21]
    >>> r = range(4,9)
    >>> l = list(r)
    >>> l
    [4, 5, 6, 7, 8]
    >>> int("4")
    4
    >>> float(3)
    3.0
    >>> str(4)
    '4'
    >>> a = 45
    >>> print("a vaut : "+a)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: can only concatenate str (not "int") to str
    >>> print("a vaut : "+str(a))
    a vaut : 45
    >>> bool()
    False
    >>> bool(2)
    True
    >>> r= range(l)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'list' object cannot be interpreted as an integer
    >>> l1 = [1,3,5,6,9,8,7]
    >>> l2 = [n*n for n in l1]
    >>> l2
    [1, 9, 25, 36, 81, 64, 49]
    >>> l3 = [str(n) for n in l1]
    >>> l3
    ['1', '3', '5', '6', '9', '8', '7']
    >>> #tuple
    >>> t1 = (2,4,2,6,3)
    >>> t1[3]
    6
    >>> t1[3] = 18
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'tuple' object does not support item assignment
    >>> lt1 = list(t1)
    >>> t1 += (2,1,2)
    >>> t1
    (2, 4, 2, 6, 3, 2, 1, 2)
    >>> t1 += (9)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: can only concatenate tuple (not "int") to tuple
    >>> t1 += 9
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: can only concatenate tuple (not "int") to tuple
    >>> t1 += ("9")
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: can only concatenate tuple (not "str") to tuple
    >>> t1 += (9,)
    >>> t1
    (2, 4, 2, 6, 3, 2, 1, 2, 9)
    >>> t1 +=()
    >>> t1
    (2, 4, 2, 6, 3, 2, 1, 2, 9)
    >>> s1 = {8,5,4,6,9,7,1}
    >>> s1
    {1, 4, 5, 6, 7, 8, 9}
    >>> s1 += {20,1}
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unsupported operand type(s) for +=: 'set' and 'set'
    >>> s1.add(13)
    >>> s1.add(55)
    >>> s1
    {1, 4, 5, 6, 7, 8, 9, 13, 55}
    >>> for i in s1: print(i)
    ...
    1
    4
    5
    6
    7
    8
    9
    13
    55
    >>> s1 & {2,4,6}
    {4, 6}
    >>> s1 - {2,4,6}
    {1, 5, 7, 8, 9, 13, 55}
    >>> fs1 = frozenset(s1)
    >>> fs1
    frozenset({1, 4, 5, 6, 7, 8, 9, 13, 55})
    >>> fs1.add(12)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: 'frozenset' object has no attribute 'add'
    >>> #liste double
    >>> ss1 = {{1,2},{3,4}}
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unhashable type: 'set'
    >>> ss1 = [[1,2],[3,4]]
    >>> ll1 = [[1,2],[3,4]]
    >>> ll1[1][1]
    4
    >>> ll1[0][0]
    1
    >>> ll1
    [[1, 2], [3, 4]]
    >>> len(ll1)
    2
    >>> ls1 = [{9,1},{4,3,2,4}]
    >>> len(ls1)
    2
    >>> len(ls1[1])
    3
    >>> lb1 = [1, [3,4], {"a","c"}, [4], [[5]] ]
    >>> {2.0,2.0}
    {2.0}
    >>> {2.0,2}
    {2.0}
    >>> lb1 = [1, [3,4], {"a","c"}, [4], [[5]] ]
    >>> lb1
    [1, [3, 4], {'a', 'c'}, [4], [[5]]]
    >>> ss2 = {[1,2],[4,7]}
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unhashable type: 'list'
    >>> ss2 = {(1,2),(4,7)} # set dans tuple
    >>> ss2
    {(1, 2), (4, 7)}
    >>> ss2 = {(1,2),(1,2)} # set dans tuple
    >>> ss2
    {(1, 2)}
    >>> {1,2},{4,7} # intersection
    ({1, 2}, {4, 7})
    >>> {1,2} & {2,7} # intersection
    {2}
    >>> {1,2} | {2,7} # union
    {1, 2, 7}
    >>> {1,2} - {2,7} # union
    {1}
    >>> {1,2} - {2,7} # difference
    {1}
    >>> {1,2} ^ {2,7} # difference simetrique
    {1, 7}
    >>> {1,2} ^ {",7} # difference simetrique
      File "<stdin>", line 1
        {1,2} ^ {",7} # difference simetrique
                                             ^
    SyntaxError: EOL while scanning string literal
    >>> {1,2} ^ {3,7} # difference simetrique
    {1, 2, 3, 7}
    >>> capteur1 = {"fleche", "bord", "arrete"}
    >>> len(capteur1)
    3
    >>> i & 1
    1
    >>> i=0
    >>> i & 1
    0
    >>> 2&1
    0
    >>> 3&1
    1
    >>> 4&1
    0
    >>> #Hexa
    >>> b1 = b"abc"
    >>> b1
    b'abc'
    >>> b1.hex()
    '616263'
    >>> b1[4]
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    IndexError: index out of range
    >>> b1[4] = 25
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'bytes' object does not support item assignment
    >>> ba1 = bytearray(b1)
    >>> ba1
    bytearray(b'abc')
    >>> ba1[2]
    99
    >>> ba1[2] = 41
    >>> ba1[2] = 65
    >>> ba1
    bytearray(b'abA')
    >>> ba1 += b"x"
    >>> ba1
    bytearray(b'abAx')
    >>> ba1[3]=2
    >>> ba1
    bytearray(b'abA\x02')
    >>> ba1[0]
    97
    >>> ba1[1]
    98
    >>> ba1[2]
    65
    >>> ba1[3]
    2
    >>> ba1[3] = 300
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    ValueError: byte must be in range(0, 256)
    >>> d1 = { "x" : 1443, "y" : 1879}
    >>> for x in d1 : print(x)
    ...
    x
    y
    >>> d1["z"] = 1422
    >>> 1879 in d1
    False
    >>> "x" in d1
    True
    >>> 1443 in d1
    False
    >>> "z" in d1
    True
    >>> d1
    {'x': 1443, 'y': 1879, 'z': 1422}
    >>> #fonction
    >>> def f1(): print("ok")
    ...
    >>> f1()
    ok
    >>> def f2(a,b,c): print("ok : ", a,b,c)
    ...
    >>> f2(4, "x", 9)
    ok :  4 x 9
    >>> f3(a, b=5, c=0): print("tot : ", a+b+c)
      File "<stdin>", line 1
        f3(a, b=5, c=0): print("tot : ", a+b+c)
        ^
    SyntaxError: illegal target for annotation
    >>> def f3(a, b=5, c=0): print("tot : ", a+b+c)
    ...
    >>> f3(1,1,1)
    tot :  3
    >>> f3(1,1)
    tot :  2
    >>> f3(1)
    tot :  6
    >>> f3(a=1, c=1)
    tot :  7
    >>> f3(c=1, a=1)
    tot :  7
    >>> print("aaa")
    aaa
    >>> print("aaa",end="-")
    aaa->>>
    >>> def f4(x,y):
    ...     z = f+y
    ...     z = x+y
    ...     return z
    ... f4(3,5)
      File "<stdin>", line 5
        f4(3,5)
        ^
    SyntaxError: invalid syntax
    >>> def f4(x,y):
    ...     z = x+y
    ...     return z
    ... f4(3,5)
      File "<stdin>", line 4
        f4(3,5)
        ^
    SyntaxError: invalid syntax
    >>> f4(3,5)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'f4' is not defined
    >>> def f4(x,y):
    ...     z = x+y
    ...     return z
    ...
    >>> f4(3,5)
    8
    >>> a = f4(3,5)
    >>> a
    8
    >>> b = f4(f4(1,2), f4(3,4))
    >>> b
    10
    >>> def f5(x,y): return x*y
    ...
    >>> f5 = lambda x,y: x*y
    >>> f5(2,3)
    6
    >>> l1 = [3,4,3,2,1,4]
    >>> l2 = filter(lambda x:x<3, l1)
    >>> l2
    <filter object at 0x000001D980E7A100>
    >>> list(12)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'int' object is not iterable
    >>> list(l2)
    [2, 1]



#******************************************************************************
#*************************"Formation Python : 17/03/2021"**********************
#******************************************************************************
print("Formation Python : 17/03/2021")
    >>> class Personne:
    ...     nom = ""
    ...     age = 10
    ...
    >>> p1
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'p1' is not defined
    >>> p1 = Personne()
    >>> p1.nom
    ''
    >>>
    >>> p1.age
    10
    >>> class Personne:
    ...     nom = ""
    ...     age = 10
    ...     def vieillir(self):
    ...             self.age += 1
    ...
    >>> p1 = Personne()
    >>> p1.age
    10
    >>> p1.vieillir()
    >>> p1.age
    11
    >>> # Exercise
    >>> # Créer une une classe capteur avec un nom, une valeur min, actuelle et max ; y créer une methode qui affiche le capteur sous la forme
    >>> # --->> "capteur 8 |##########---|" et tester
    >>>
    >>> #Class et Constructeurs
    >>> class Personne:
    ...     def __init__(self):
    ...             self.nom = ""
    ...             self.age = 10
    ...     def vieuillir(self): self.age += 1
    ...
    >>> p1 = Personne()
    >>> p1.age
    10
    >>> class Personne:
    ...     def __init__(self,n):
    ...             self.nom = n
    ...             self.age = 10
    ...     def vieuillir(self):
    ...             self.age += 1
    ...
    >>> p2 = Personne()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: __init__() missing 1 required positional argument: 'n'
    >>> p2 = Personne("Tim")
    >>> p2.nom
    'Tim'
    >>> class Personne:
    ... ...     def __init__(self,n):
      File "<stdin>", line 2
        ...     def __init__(self,n):
        ^
    IndentationError: expected an indented block
    >>> ...             self.nom = n
      File "<stdin>", line 1
        ...             self.nom = n
                        ^
    SyntaxError: invalid syntax
    >>> ...             self.age = 10
      File "<stdin>", line 1
        ...             self.age = 10
                        ^
    SyntaxError: invalid syntax
    >>> class Personne:
    ...     def __init__(self,n):
    ...             self.nom = n
    ...             self.age = 10
    ...     def vieuillir(self): self.age += 1
    ...     def __del__(self): print("del!")
      File "<stdin>", line 6
    TabError: inconsistent use of tabs and spaces in indentation
    >>> class Personne:
    ...     def __init__(self,n):
    ...             self.nom = n
    ...             self.age = 10
    ...     def vieuillir(self): self.age += 1
    ...     def __del__(self): print("del!")
      File "<stdin>", line 6
    TabError: inconsistent use of tabs and spaces in indentation
    >>>
    >>> class Personne:
    ...     def __init__(self,n):
    ...             self.nom = n
      File "<stdin>", line 3
    TabError: inconsistent use of tabs and spaces in indentation
    >>> class Personne:
    ...     def __init__(self,n):
    ...             self.nom = n
    ...             self.age = 10
    ...     def vieuillir(self): self.age += 1
    ...     def __del__(self): print("del!")
    ...
    >>> def f(): p1=Personne("Ann")
    ...
    >>> f()
    del!
    >>> p1 = Personne("Dana")
    >>> p1.nom
    'Dana'
    >>> #Add qqch sauf les nombres
    >>> class Personne:
    ...     def __init__(self,n):
    ...             self.nom = n
    ...             self.age = 10
    ...     def vieuillir(self): self.age += 1
    ...     def __add__(self,v):
    ...             p2 = Personne(self.nom)
    ...             p2.age = self.age + v
    ...             return p2
    ...
    >>>
    >>>
    >>>
    >>>
    >>> # _____________Classe____________
    >>> class Marin(Personne):
    ...     pass
    ...
    >>> m1 = Marin("Eric")
    >>> m1.nom
    'Eric'
    >>> class Marin(Personne):
    ...     def __init__(self,n):
    ...             Personne.__init__(self,n)
    ...             self.bateau = ""
    ...
    >>> #_______________________________
    >>> #
    >>> #
    >>> #
    >>> textes = (
    ... "Ce text est un texte normal en francais, sans particularités",
    ... "This is an english text, writtent by an english person",
    ... "My tailor is rich, Ben is in the kitchen, Look, it's a plane, a bird, no it's superman"
    ... )
    >>> textes[1]
    'This is an english text, writtent by an english person'
    >>> len(textes[0])
    60
    >>> textes[0].find("w")
    -1
    >>> textes[1].find("w")
    25
    >>> textes[1].find("w")
    25
    >>> textes[1].find("w")
    25
    >>>
    >>> textes[1].find("w")
    25
    >>> textes[1].find("e")
    11
    >>> textes[0][1]
    'e'
    >>> w_cnt=0
    >>> for s in range(len(textes[0])):
    ...     if textes[0][s] == "w"
      File "<stdin>", line 2
        if textes[0][s] == "w"
                              ^
    SyntaxError: invalid syntax
    >>>         w_cnt += 1w_cnt=0
      File "<stdin>", line 1
        w_cnt += 1w_cnt=0
    IndentationError: unexpected indent
    >>> for s in range(len(textes[0])):
    ...     if textes[0][s] == "w":
    ...         w_cnt += 1
    ...
    >>>
    >>>         w_cnt
      File "<stdin>", line 1
        w_cnt
    IndentationError: unexpected indent
    >>> w_cnt
    0
    >>> w_cnt=0
    >>> for s in range(len(textes[0])):
    ...     if textes[0][s] == 'w':
    ...         w_cnt += 1
    ...
    >>> w_cnt
    0
    >>> textes.replace(' ','')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: 'tuple' object has no attribute 'replace'
    >>> texte[0].replace(' ','')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'texte' is not defined
    >>> textes[0].replace(' ','')
    'Cetextestuntextenormalenfrancais,sansparticularités'
    >>>
    >>> textes = (
    ... "Ce text est un texte normal en francais, sans particularités, si ce n'est peu d'accents",
    ... "This is an english text, well writtent by an english person",
    ... "My tailor is rich, Ben is in the kitchen, Look, it's a plane, a bird, no it's superman"
    ... )
    >>> len(textes.replace(' ',''))
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: 'tuple' object has no attribute 'replace'
    >>> len(textes[0].replace(' ',''))
    73
    >>>




#******************************************************************************
#*************************"Formation Python : 18/03/2021"**********************
#******************************************************************************
print("Formation Python : 18/03/2021")

# Importation de fichiers
import math
math.trunc(5.4) # 5

from random import randint
randint(3,6) # 4

from random import randint, choice
choice([1,3,4,5]) # 4

from zlib import *
compress(b"lsk") # b'x\x9c\xcb)\xce\x06\x00\x02\x98\x01K'

import re
re.match("abc", "abcabc")
re.match("abc", "abcabc") != None
re.match("abc", "xyzxyz") != None
re.match("abc", "xyzabc") != None
re.match("abc", "abcxyz") != None
re.match("a?b+c", "abcxyz") != None # ? = 0..1     + = 1..oo    * = 0..oo
re.match("..c\s\d", "abcxyz") != None
re.match("a{1}b{1,8}", "abcxyz") != None # . = n'importe quoi, \s : espace, \d : digit
re.match("[abccdef]{3}", "abcxyz") != None
re.match("[a-f]{3}", "abcxyz") != None
re.match("[^0-9]{3}", "abcxyz") != None
re.match("[a-z0-9\.\-]+@[a-z0-9\.\-]+", "abcxyz") != None
re.match(".*z.*", "ma chaine") != None # ma chaine contient un z ?
re.match(".*[0-9]", "ma chaine") != None # ma chaine contient au moins un chiffre
re.match("[0-9]{5}", "ma chaine") != None # code postal 
re.match("[A-Z][a-z]", "ma chaine") != None # Nom propre


##DATETIME

import datetime
maintenant = datetime.datetime.now() # datetime.datetime(2021, 3, 18, 10, 58, 31, 438017)
prochaineferie = datetime.datetime(2021,4,5) # datetime.datetime(2021, 4, 5, 0, 0)

troisjours = datetime.timedelta(days=3)
prochaineferie - troisjours - troisjours # datetime.datetime(2021, 3, 30, 0, 0)

troisjours = datetime.timedelta(hours=24*3)
prochaineferie - troisjours - troisjours # datetime.datetime(2021, 3, 30, 0, 0)

troisjours # datetime.timedelta(days=3)

prochaineferie - maintenant # datetime.timedelta(days=17, seconds=46888, microseconds=561983)

prochaineferie.day # 5
prochaineferie.month # 4
prochaineferie.minute # 58

## MATH

math.sin(3)
c = 3 + 2j # (3+2j)
    # Matrice, séries de nombres : Numpy
    # Tableaux de données : Pandas
    # Machine learning : Scikit Learn ... traitement de données. Intelligence Artificiel
    # Graphes : MathPlotLib

## SYSTEME

import sys
sys.argv # ['']
sys.modules
sys.modules['re']
# ou elle va chercher les modules ??
sys.path # il montre les path où il cherche les modules 
sys.path.append("C:\\lecaroacosta\\")


#******************************************************************************
#*************************"Formation Python : 19/03/2021"**********************
#******************************************************************************

El archivo 6_classe es copiado al archivo text_dic
modificacion en el archivo: text_dic
__name__ : significa que yo estoy ejecutando directamente este archivo
__name__ contiene __main__


version cProfile --->> version hecha en C
version Profile  --->> version hecha en python

def slow1():
    a = []
    for i in range(100000): a.append(i)
    
def slow2():
    for i in range(10): slow1()
    
slow2()
cProfile.run('slow2()')
         1000014 function calls in 0.399 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
       10    0.261    0.026    0.366    0.037 <stdin>:1(slow1)
        1    0.033    0.033    0.399    0.399 <stdin>:1(slow2)
        1    0.000    0.000    0.399    0.399 <string>:1(<module>)
        1    0.000    0.000    0.399    0.399 {built-in method builtins.exec}
  1000000    0.104    0.000    0.104    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}

profile.run('slow2()')
         1000015 function calls in 2.234 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
  1000000    1.203    0.000    1.203    0.000 :0(append)
        1    0.000    0.000    2.234    2.234 :0(exec)
        1    0.000    0.000    0.000    0.000 :0(setprofile)
       10    1.016    0.102    2.219    0.222 <stdin>:1(slow1)
        1    0.016    0.016    2.234    2.234 <stdin>:1(slow2)
        1    0.000    0.000    2.234    2.234 <string>:1(<module>)
        0    0.000             0.000          profile:0(profiler)
        1    0.000    0.000    2.234    2.234 profile:0(slow2())











