# Traitement sur les chaines

textes = (
	"Ce text est un texte normal en francais, sans particularités, si ce n'est peu d'accents",
	"This is an english text, well writtent by an english person",
	"My tailor is rich, Ben is in the kitchen, Look, it's a plane, a bird, no it's superman"
)

# differences :
# anglais : 2% de w, 2% de y, 2.7% de u, 0.1% de q, %6 de h
# francais : 0% de w, 0.1% de y, 6% de u, 1.4% de q, 0.6% de h
# algorithme qui dit si chaque text est probablement francais ou anglais :

#for txt in [1:2]:
# w_cnt=0 
# y_cnt=0 
# u_cnt=0 
# q_cnt=0 
# h_cnt=0
# txt = 0
# text0 = textes[0].replace(' ','')
# lth = len(text0)
# for s in range(len(text0)):
    # if textes[txt][s] == 'w':
        # w_cnt += 1
    # if textes[txt][s] == 'y':
        # y_cnt += 1
    # if textes[txt][s] == 'u':
        # u_cnt += 1
    # if textes[txt][s] == 'q':
        # q_cnt += 1
    # if textes[txt][s] == 'h':
        # h_cnt += 1
# #lth = len(textes[0].replace(' ',''))
# print(w_cnt * 0 / lth)
# print(y_cnt * 0.1 / lth)
# print(u_cnt * 6 / lth)
# print(q_cnt * 1.4 / lth)
# print(h_cnt * 0.6 / lth)

#*********************Solution formateur****************************

stats_prc = {
    'fr':{'w':0, 'y':0.1, 'u':6, 'q':1.4, 'h':0.6},
    'en':{'w':2, 'y':2, 'u':2.7, 'q':0.1, 'h':6}
}

lettres_utiles = ('h','q','u','w','y')
for texte in textes:
    pourcents = {}
    for l in lettres_utiles:
        pourcents[l] = 0
    for c in texte.lower():
        if c in lettres_utiles:
            pourcents[c] += 1/len(texte.replace(' ',''))
    print(" * "+texte)
    print(pourcents)
    if pourcents['h']>pourcents["u"]:
        print("Anglais")
    else:
        print("Français")

       
print("Syntaxe : ")
# indiquer (True/False) si la syntaxe est correcte : toute virgule est suivie d'un espace
for texte in textes:
    ok = True
    for i in range(len(texte)-1):
        if texte[i] == ',' and texte[i+1] != ' ':
            ok = False
    print(texte+" : "+str(ok))

print("Sujet : ")
# indiquer si on trouve "text" dans la phrase    
for texte in textes:
    linguistique = "text" in texte
    print(texte+" : "+str(linguistique))    

# Exercise
# Grace au regexp : indiquer si c'est des textes simples :
# - aucun mot de plus de 9 lettres
# - aucun tiret (-), point-virgule (;), guillemet (")
print("Simple : ")

import re
for texte in textes:
    simple = re.match(".*[A-Za-z]{10}", texte) == None
    if simple:
        simple = re.match(".*[;\"\-]", texte) == None
        
    #autre = re.match(".*a.*b.*c.*d.*", texte)
    print(texte+ " : "+str(simple))








        
