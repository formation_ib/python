# Test
import re

class Correction:
    def mettre_en_minuscules(self, s):
        """Met en minuscules uniquement la chaine
        >>> c = Correction()
        >>> c.mettre_en_minuscules("Hello !")
        'hello !'
        >>> s = c.mettre_en_minuscules("ABC")
        >>> s[0]
        'a'
        >>> s[1]
        'b'
        """
        return s.lower()
	
    def enlever_telephones(self, s):
        """Remplace le num de tel par des etoile (i.e : "redaction")
        >>> c = Correction()
        >>> c.enlever_telephones("re0504897184")
        're***'
        >>> c.enlever_telephones("Ce test va enlever le numero de telephone 89457845787")
        'Ce test va enlever le numero de telephone ***'
        """
        return re.sub("[0-9]+", "***", s)

if __name__=="__main__":
    import doctest
    doctest.testmod()
    print("Test finis !")