#******************************************************************************
#*************************"Formation Python : 16/03/2021"**********************
#******************************************************************************

capteur1 = {"fleche", "bord", "arrete"} # pas d'ordre
capteur2 = {"fleche", "entree"}
capteur3 = {"sortie 2", "entree", "fleche"}

signaux = [capteur1, capteur2, capteur3]

if False:
    # print("Signaux actifs sur tous les capteurs")
    ##fleche
    # sig = capteur1 & capteur2 & capteur3
    # print("Signal activée : ", sig)

    # print("Signaux actifs au moins un capteur")
    ##fleche, ...
    # sig = capteur1 - capteur2 - capteur3
    # print("Signaux activées C1 : ", sig)

    # sig = capteur2 - capteur3 - capteur1
    # print("Signaux activées C2: ", sig)

    # sig = capteur3 - capteur1 - capteur2
    # print("Signaux activées C3: ", sig)

    # print("Signaux actifs sur nombre impair de capteur")
    ##fleche, bord, arret, sortie 2
    # print("Capteur 1")
    # lth= len(capteur1)
    # for i in range(lth):
        # if(i & 1):
            # print(capteur1[i])
            
    ## Solution Formateur
    print("Signaux actifs sur tous les capteurs :")
    sa1 = capteur1
    for capteur in signaux[1:]:
        sa1 = sa1 & capteur
    print(sa1)     # fleche

    print("Signaux actifs au moins un capteur :")
    sa2 = capteur1
    for capteur in signaux[1:]:
        sa2 = sa2 | capteur
    print(sa2)     # fleche, bord, arrete, bord, entree, sortie 2

    print("Signaux actifs sur un nombre impair de capteurs :")
    sa3 = capteur1
    for capteur in signaux[1:]:
        sa3 = sa3 ^ capteur
    print(sa3)     # fleche, bord, arrete, sortie 2

    print("Signaux actifs sur un nombre pair de capteurs :")
    sa4 = sa2 - sa3
    print(sa4) # entree

    # Algebre ensembliste :
    # >>> { (1,), (1,) }
    # {(1,)}
    # >>> {1,2} & {1,3}
    # {1}
    # >>> {1,2} | {1,3}
    # {1, 2, 3}
    # >>> {1,2} - {1,3}
    # {2}
    # >>> {1,2} ^ {1,3}

print("Nombre de fois qu'apparait chaque signal :")
nbsig = {} # clés : str, valeurs : int
for capteur in signaux:
    for signal in capteur:
        if signal in nbsig:
            nbsig[signal] += 1
        else:
            nbsig[signal] = 1
print(nbsig) # entree : 2, fleche: 3...






















