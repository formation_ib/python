#******************************************************************************
#*************************"Formation Python : 15/03/2021"**********************
#******************************************************************************
print("Formation Python : 15/03/2021")

# un operateur lit un code d'erreur sur un appareil et l'analuse ici

# try:
    # n = int(input("code ?"))
# except:
    # print("Entrez le bon code")
# else:
    # print("Code d'erreur : ", n)
    
    
# Another option
try:
    n = int(input("code ?"))
    print("Code d'erreur : ", n)
    
    # exemple :
    # 1 => erreurs 1
    # 2 => erreurs 2
    # 4 => erreurs 3
    # 8 => erreurs 4
    # 5 => erreurs 1 et 3
    # 10 => erreurs 2 et 4
    # 7 => erreurs 1 et 2 et 3
    
    if n/8 >= 1:
        res = n//8
        res_div = (n/8 - n//8)*8
            
        for i in range(res):
            print("Erreurs 4")   
        if res_div == 4:
            print("Erreurs 3")
            
        if res_div == 2:
            print("Erreurs 2")
            
        if res_div == 1:
            print("Erreurs 1")
            
        if res_div/4 >= 1:
            res1 = res_div//4
            res_div = (res_div/4 - res_div//4)*4
                
            if (res1*4) == 4:
                print("Erreurs 3")
                
            if res_div == 3:
                print("Erreurs 2")
                print("Erreurs 1")
                
            if res_div == 2:
                print("Erreurs 2")
                
            if res_div == 1:
                print("Erreurs 1")
        elif res_div/3 >= 1:
                print("Erreurs 2")
                print("Erreurs 1")
        elif res_div/2 >= 1:
            print("Erreurs 2")
        elif res_div/1 == 1:
            print("Erreurs 1")
            
    elif n/4 >= 1:
        res = n//4
        res_div = (n/4 - n//4)*4
            
        if (res*4) == 4:
            print("Erreurs 3")
            
        if res_div == 3:
            print("Erreurs 2")
            print("Erreurs 1")
            
        if res_div == 2:
            print("Erreurs 2")
            
        if res_div == 1:
            print("Erreurs 1")
    elif n/3 >= 1:
            print("Erreurs 2")
            print("Erreurs 1")
    elif n/2 >= 1:
        print("Erreurs 2")
    else:
        print("Erreurs 1")
    
except ValueError:
    print("ValueError !")
    
#Deuxieme option excercise
try:
    n = int(input("Code ?"))
    print("Code d'erreur : ", n)
    
    # exemple :
    # 1 => erreurs 1
    # 2 => erreurs 2
    # 4 => erreurs 3
    # 8 => erreurs 4
    # 5 => erreurs 1 et 3
    # 10 => erreurs 2 et 4
    # 7 => erreurs 1 et 2 et 3
    num = 0
    bit = 1
    s = ""
    sep = ""
    # complexité : O(log(n))
    while num < n:
        num = 2**(bit-1)
        if num & n != 0:
            s += sep+str(bit)
            sep = " et "
        bit += 1
    print(s)
    # complexités dans l'ordre : O(1), O(log(n)), O(n), O(n*log(n)), O(n²), O(n**3)...
    
except ValueError:
    print("ValueError !")
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    